defmodule Monitor do
  @me __MODULE__

  defstruct(new_games: 0)

  def start_link() do
    Agent.start_link(fn -> %Monitor{} end, name: @me)
  end

  def new_games() do
    Agent.get(@me, fn monitor -> monitor.new_games end)
  end
end
